program MengolahDataMahasiswa;
uses
    crt;
const
     maks_array = 50;
type
    Larik1 = array[1..maks_array] of string;
    Larik2 = array[1..maks_array] of integer;
    Larik3 = array[1..maks_array] of char;
var
   n : integer;
   nim, nama : Larik1;
   nilai : Larik2;
   indeks : Larik3;
procedure wp(x,y:integer;text:string);
begin
   gotoxy(x,y);
   writeln(text);
end;//endprocedure

procedure JumlahData(var N : integer);
{I.S.: user memasukkan banyaknya data mahasiswa(n)}
{F.S.: menghasilkan banyaknya data mahasiswa(n)}
begin
     write('Banyaknya Data Mahasiswa : ');
     readln(N);
     while(N <= 0) or (n > maks_array) do
     begin
         writeln('Banyaknya Data Hanya Antara 1-',maks_array);
         readln;
         gotoxy(1,2);clreol;
         gotoxy(28,1);clreol;readln(N);
     end;
end;

function IndeksNilai(nilai : integer):char;
{I.S.: Nilai Sudah Terdefenisi}
{F.S.: Menghasilkan fungsi indeks nilai}
begin
   case nilai of
     80..100 : IndeksNilai := 'A';
     70..79  : IndeksNilai := 'B';
     60..69  : IndeksNilai := 'C';
     50..59  : IndeksNilai := 'D';
     0..49   : IndeksNilai := 'E';
   end; //endcase
end;//endfunction

function RataRataNilai(n:integer;total:real):real;
{I.S.:}
{F.S.:}
begin
  RataRataNilai := total / n;
end;//endfunction

procedure TampilData(n:integer);
{I.S.: Banyaknya data sudah terdefenisi}
{F.S.: Menampilkan daftar nilai mahasiswa}
var
   i,total:integer;
begin
   total := 0;
   clrscr;
   wp(29,1,'DAFTAR NILAI MAHASISWA');
   wp(11,2,'--------------------------------------------------------');
   wp(11,3,'| NO |   N I M   |   NAMA MAHASISWA   | NILAI | INDEKS |');
   wp(11,4,'--------------------------------------------------------');
   for i:= 1 to n do
   begin
     wp(11,i+4,'|    |           |                    |       |        |');
     gotoxy(13,i+4);write(i);
     gotoxy(18,i+4);readln(nim[i]);
     gotoxy(30,i+4);readln(nama[i]);
     gotoxy(52,i+4);readln(nilai[i]);
     //validasi nilai
     while(nilai[i] <= 0) do
     begin
        gotoxy(11,i+6);write('Nilai Tidak Boleh < 1, tekan enter untuk mengisi ulang');
        readln;
        gotoxy(11,i+6);clreol;
        gotoxy(52,i+4);readln(nilai[i]);
     end;
     indeks[i] := IndeksNilai(nilai[i]);
     gotoxy(62,i+4);write(indeks[i]);
     total := total + nilai[i];
   end;//endfor
   gotoxy(11,i+5);
   writeln('--------------------------------------------------------');
   gotoxy(11,i+6);
   writeln('Rata-rata nilai dari ',n,' data = ', RataRataNilai(n,total):0:1);
end; //endprocedure

{Algoritma Utama}
begin
     JumlahData(n);
     TampilData(n);
     readln;
end.
