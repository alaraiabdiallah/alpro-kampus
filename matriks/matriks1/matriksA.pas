program Membuat_Matriks_A;
uses
    crt;
type
    matriks = array[1..5,1..3] of integer;
var
   a : matriks;
procedure IsiMatriks(var a:matriks);
var
   i,j:integer;
begin
   writeln('Memasukkan Matriks A');
   writeln('--------------------');
   for i := 1 to 5 do
   begin
       for j := 1 to 3 do
       begin
           gotoxy(j*9-8,i+2);readln(a[i,j]);
       end;
   end;
end;

procedure TampilMatriks(a:matriks);
var
   i,j:integer;
begin
   clrscr;
   writeln('Hasil Matriks Berordo 5x3');
   writeln('-------------------------');
   for i := 1 to 5 do
   begin
       for j := 1 to 3 do
       begin
           gotoxy(j*10-9,i+2);delay(800);writeln(a[i,j]);
       end;
   end;
end;

begin
   IsiMatriks(a);
   TampilMatriks(a);
   readln;
end.
