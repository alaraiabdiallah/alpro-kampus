program latihan_tugas;

uses 
	crt;
	
const
	maks_baris = 50;
	maks_kolom = 5;
type
	MKRecord = record
		kode,nama : string;
		sks		  : integer;
	end;

	MhsRecord = record
		nim,nama : string;
		ipk		 : real;
	end;

	Matriks1 = array[1..maks_baris,1..maks_kolom] of integer;
	Matriks2 = array[1..maks_baris,1..maks_kolom] of char;
	Mahasiswa = array[1..maks_baris] of MhsRecord;
	Matkul   = array[1..maks_kolom] of MKRecord;
var
	jml_mhs, jml_mk : integer;
	nilai 			: Matriks1;
	indeks			: Matriks2;
	mhs   			: Mahasiswa;
	mk    			: Matkul;

function IndeksNilai(nilai : integer):char;
{I.S.: Nilai Sudah Terdefenisi}
{F.S.: Menghasilkan fungsi indeks nilai}
begin
   case nilai of
     80..100 : IndeksNilai := 'A';
     70..79  : IndeksNilai := 'B';
     60..69  : IndeksNilai := 'C';
     50..59  : IndeksNilai := 'D';
     0..49   : IndeksNilai := 'E';
   end; //endcase
end;//endfunction

function BobotNilai(indeks : char):integer;
{I.S.: Indeks Sudah Terdefenisi}
{F.S.: Menghasilkan fungsi Bobot Nilai}
begin
   case indeks of
     'A'  : BobotNilai := 4;
     'B'  : BobotNilai := 3;
     'C'  : BobotNilai := 2;
     'D'  : BobotNilai := 1;
     'E'  : BobotNilai := 0;
   end; //endcase
end;//endfunction

function HitungIPK(id,n:integer; mk:Matkul; indeks: Matriks2):real;
var
	i,TotalSKS,TotalBobot :integer;
begin
	TotalSKS := 0;
	TotalBobot := 0;
	for i := 1 to n do
	begin
		TotalSKS   := TotalSKS + mk[i].sks;
		TotalBobot := TotalBobot + (BobotNilai(indeks[id,i]) * mk[id].sks);
	end;
	HitungIPK := TotalBobot / TotalSKS;
end;


procedure jumlah_data(var jml_mhs,jml_mk : integer);
begin
	clrscr;
	write('Banyaknya Mahasiswa : ');readln(jml_mhs);
	while (jml_mhs <= 0) or (jml_mhs > maks_baris) do
	begin
		writeln('Jumlah Mahasiswa 1-',maks_baris);
		readln;
		gotoxy(1,2);clreol;
		gotoxy(1,1);clreol;
		write('Banyaknya Mahasiswa : ');readln(jml_mhs);
	end;
	write('Banyaknya Mata Kuliah : ');readln(jml_mk);
	while (jml_mk <= 0) or (jml_mk > maks_kolom) do
	begin
		writeln('Jumlah Mata Kuliah 1-',maks_kolom);
		readln;
		gotoxy(1,3);clreol;
		gotoxy(1,2);clreol;
		write('Banyaknya Mata Kuliah : ');readln(jml_mk);
	end;
end;

procedure Isi_data;
{I.S:}
{F.S:}
var
  i,j, baris: integer;
begin
  jumlah_data(jml_mhs,jml_mk);
  clrscr;
  baris := 0; // inisialisasi awal
  gotoxy(11,1);
  writeln('DAFTAR MAHASISWA');
  writeln('---------------------------------------');
  writeln('| NO |   N I M   |   NAMA MAHASISWA   |');
  writeln('---------------------------------------');
  for i := 1 to jml_mhs do
  begin
  	gotoxy(1,i+4);
  	writeln('|    |           |                    |');
  	gotoxy(3,i+4);write(i); // nomor
  	gotoxy(8,i+4);readln(mhs[i].nim);
   	gotoxy(21,i+4);readln(mhs[i].nama);
  end;
  gotoxy(1,i+5); // i = 10
  writeln('---------------------------------------');
  //Isi Matkul
  baris := baris + i + 6;

  // gotoxy(col, row); 
  gotoxy(15,baris+1); writeln('DAFTAR MATA KULIAH');
  gotoxy(1,baris+2); writeln('-------------------------------------------------');
  gotoxy(1,baris+3); writeln('| NO |   KODE   |   NAMA MATAKULIAH   |   SKS   |');
  gotoxy(1,baris+4); writeln('-------------------------------------------------');

  baris := baris + 4;
  for j := 1 to jml_mk do
  begin
  	gotoxy(1,j+baris);
  	writeln('|    |          |                     |         |');
  	gotoxy(3,j+baris);write(j);
  	gotoxy(9,j+baris);readln(mk[j].kode);
  	gotoxy(20,j+baris);readln(mk[j].nama);
  	gotoxy(42,j+baris);readln(mk[j].sks);
  end;
  baris := baris + j;
  gotoxy(1,baris + 1);
  writeln('-------------------------------------------------');
  gotoxy(1,baris + 2);
  writeln('Tekan Enter Untuk Lanjut');readln;
end;

procedure IsiNilai(m,n:integer;var mhs: Mahasiswa; mk: Matkul; var nilai:Matriks1; var indeks:Matriks2);
{I.S.:}
{F.S.:}
var
	i,j:integer;
begin
	clrscr;
	gotoxy(27,1);writeln('PENGISIAN NILAI MAHASISWA');
	gotoxy(27,2);writeln('-------------------------');
	gotoxy(13,3);write('Kode Mata Kuliah');
	gotoxy(1,4);write('NIM');
	for i := 1 to m do
	begin
		textcolor(blue);gotoxy(1,i+4);writeln(mhs[i].nim);
	end;

	for i := 1 to n do
	begin
		textcolor(blue);gotoxy(i*15-2,4);writeln(mk[i].kode);
	end;

	// isi nilai 
	for i := 1 to m do
	begin
		for j := 1 to n do
		begin
			textcolor(white);
			gotoxy(j*15-2,i+4);readln(nilai[i,j]);
			indeks[i,j] := IndeksNilai(nilai[i,j]);
			// simpen di sini
			readln;
		end;
	end;

	for i := 1 to m do
	begin
		for j := 1 to n do
		begin
			textcolor(white);
			gotoxy((j + 8) * 5,i+4);write(indeks[i,j]);
		end;
	end;

	for i := 1 to m do
	begin
		mhs[i].ipk := HitungIPK(i, n, mk, indeks);
	end;
end;

begin
	Isi_data;
	IsiNilai(jml_mhs, jml_mk, mhs, mk, nilai, indeks);
end.