program mengelola_nilai_mahasiswa;
{I.S:}
{F.S:}
uses
   crt;
const
   MaksMhs = 50;
   MaksMK  = 5;

type
   MhsRecord = record
      nim, nama : string;
      ipk 		: real;
   end;
   
   MKRecord  = record
      kode, nama: string;
      sks       : integer;
   end;
   
   Matriks1  	 = array[1..MaksMhs,1..MaksMK] of integer;
   Matriks2	 	 = array[1..MaksMhs,1..MaksMK] of char;
   Mahasiswa     = array[1..MaksMhs] of MhsRecord;
   Matkul        = array[1..MaksMK]  of MKRecord;

var
  m, n  : integer;
  mhs   : Mahasiswa;
  mk    : Matkul;
  nilai : Matriks1;
  indeks: Matriks2;

function IndeksNilai(nilai : integer):char;
{I.S.: Nilai Sudah Terdefenisi}
{F.S.: Menghasilkan fungsi indeks nilai}
begin
   case nilai of
     80..100 : IndeksNilai := 'A';
     70..79  : IndeksNilai := 'B';
     60..69  : IndeksNilai := 'C';
     50..59  : IndeksNilai := 'D';
     0..49   : IndeksNilai := 'E';
   end; //endcase
end;//endfunction

function BobotNilai(indeks : char):integer;
{I.S.: Indeks Sudah Terdefenisi}
{F.S.: Menghasilkan fungsi Bobot Nilai}
begin
   case indeks of
     'A'  : BobotNilai := 4;
     'B'  : BobotNilai := 3;
     'C'  : BobotNilai := 2;
     'D'  : BobotNilai := 1;
     'E'  : BobotNilai := 0;
   end; //endcase
end;//endfunction

function HitungIPK(id,n:integer; mk:Matkul; indeks: Matriks2):real;
var
	i,TotalSKS,TotalBobot :integer;
begin
	TotalSKS := 0;
	TotalBobot := 0;
	for i := 1 to n do
	begin
		TotalSKS   := TotalSKS + mk[i].sks;
		TotalBobot := TotalBobot + (BobotNilai(indeks[id,i]) * mk[id].sks);
	end;
	HitungIPK := TotalBobot / TotalSKS;
end;

procedure JumlahData(var m,n:integer);
{I.S:}
{F.S:}
begin
  clrscr;
  write('Masukkan Jumlah Mahasiswa   : ');readln(m);
  while (m <= 0) or (m > MaksMhs) do
  begin
    writeln('Jumlah Mahasiswa Hanya 1 - ',MaksMhs,', Tekan Enter Untuk Ulangi');
    readln;
    gotoxy(1,2);clreol;
    gotoxy(1,1);clreol;
    write('Masukkan Jumlah Mahasiswa   : ');readln(m);
  end;
  
  write('Masukkan Jumlah Mata Kuliah : ');readln(n);
  while (n <= 0) or (n > MaksMK) do
  begin
    writeln('Jumlah Mahasiswa Hanya 1 - ',MaksMK,', Tekan Enter Untuk Ulangi');
    readln;
    gotoxy(1,3);clreol;
    gotoxy(1,2);clreol;
    write('Masukkan Jumlah Mata Kuliah : ');readln(n);
  end;
end;

procedure IsiData(var m,n:integer; var mhs : Mahasiswa; var mk:Matkul);
{I.S:}
{F.S:}
var
  i,j,baris: integer;
begin
  JumlahData(m,n);
  clrscr;
  baris := 0;
  gotoxy(11,1);
  writeln('DAFTAR MAHASISWA');
  writeln('---------------------------------------');
  writeln('| NO |   N I M   |   NAMA MAHASISWA   |');
  writeln('---------------------------------------');
  for i := 1 to m do
  begin
  	gotoxy(1,i+4);
  	writeln('|    |           |                    |');
  	gotoxy(3,i+4);write(i);
  	gotoxy(8,i+4);readln(mhs[i].nim);
  	gotoxy(21,i+4);readln(mhs[i].nama);
  end;
  gotoxy(1,i+5);
  writeln('---------------------------------------');
  baris := baris + i + 6;
  //Isi Matkul
  
  gotoxy(15, baris + 1);writeln('DAFTAR MATA KULIAH');
  gotoxy(1, baris + 2);writeln('-------------------------------------------------');
  gotoxy(1, baris + 3);writeln('| NO |   KODE   |   NAMA MATAKULIAH   |   SKS   |');
  gotoxy(1, baris + 4);writeln('-------------------------------------------------');
  baris := baris + 4;
  for j := 1 to n do
  begin
  	gotoxy(1,j+baris);
  	writeln('|    |          |                     |         |');
  	gotoxy(3,j+baris);write(j);
  	gotoxy(9,j+baris);readln(mk[j].kode);
  	gotoxy(20,j+baris);readln(mk[j].nama);
  	gotoxy(42,j+baris);readln(mk[j].sks);
  end;
  baris := baris + j;
  gotoxy(1,baris + 1);
  writeln('-------------------------------------------------');
  gotoxy(1,baris + 2);
  writeln('Tekan Enter Untuk Lanjutkan');readln;
end;


procedure IsiNilai(m,n:integer;var mhs: Mahasiswa; mk: Matkul; var nilai:Matriks1; var indeks:Matriks2);
{I.S.:}
{F.S.:}
var
	i,j:integer;
begin
	clrscr;
	gotoxy(27,1);writeln('PENGISIAN NILAI MAHASISWA');
	gotoxy(27,2);writeln('-------------------------');
	gotoxy(13,3);write('Kode Mata Kuliah');
	gotoxy(1,4);write('NIM');
	for i := 1 to m do
	begin
		textcolor(blue);gotoxy(1,i+4);writeln(mhs[i].nim);
	end;

	for i := 1 to n do
	begin
		textcolor(blue);gotoxy(i*15-2,4);writeln(mk[i].kode);
	end;

	for i := 1 to m do
	begin
		for j := 1 to n do
		begin
			textcolor(white);
			gotoxy(j*15-2,i+4);readln(nilai[i,j]);
			indeks[i,j] := IndeksNilai(nilai[i,j]);

		end;
	end;

	for i := 1 to m do
	begin
		mhs[i].ipk := HitungIPK(i, n, mk, indeks);
	end;
end;

procedure TampilIndeks(m,n:integer; mhs: Mahasiswa; mk: Matkul;nilai:Matriks1;indeks:Matriks2);
{I.S.:}
{F.S.:}
var
	i,j:integer;
begin
	clrscr;
	textcolor(white);
	gotoxy(27,1);delay(800);writeln('PENGISIAN NILAI MAHASISWA');
	gotoxy(27,2);delay(800);writeln('-------------------------');
	gotoxy(13,3);delay(800);write('Kode Mata Kuliah');
	gotoxy(1,4);delay(800);write('NIM');
	
	for i := 1 to n do
	begin
		textcolor(blue);gotoxy(i*15-2,4);delay(800);writeln(mk[i].kode);
	end;
	
	for i := 1 to m do
	begin
		textcolor(blue);gotoxy(1,i+4);delay(800);writeln(mhs[i].nim);
	end;

	for i := 1 to m do
	begin
		for j := 1 to n do
		begin
			textcolor(red);gotoxy(j*15-2,i+4);delay(800);writeln(indeks[i,j]); 
		end;
	end;

end;

procedure TampilHasil(m,n:integer; mhs: Mahasiswa; mk: Matkul;nilai:Matriks1;indeks:Matriks2);
{I.S:}
{F.S:}
var 
	i,j,baris:integer;
begin
	clrscr;
  baris:=0; 
	textcolor(white);
	gotoxy(6,1);writeln('HASIL STUDI MAHASISWA TEKNIK INFORMATIKA UNIKOM SEBANYAK ',m,' MAHASISWA');
	gotoxy(6,2);writeln('====================================================================');
	for i := 1 to m do
	begin
    gotoxy(1, baris + i +3);
		writeln('--------------------------------Mahasiswa Ke-',i,'---------------------------------');
		gotoxy(1, baris + i +4);writeln;
		gotoxy(1, baris + i +5);writeln('NIM    : ',mhs[i].nim);
		gotoxy(1, baris + i +6);writeln('Nama   : ',mhs[i].nama);
		gotoxy(1, baris + i +7);writeln;
		gotoxy(1, baris + i +8);writeln('------------------------------------------------------------');
		gotoxy(1, baris + i +9);writeln('| NO |   KODE   |   NAMA MATAKULIAH   |   SKS   |  INDEKS  |');
		gotoxy(1, baris + i +10);writeln('------------------------------------------------------------');
		baris := baris + i + 10;
    for j := 1 to n do
		begin
			gotoxy(1,baris + j);
			writeln('|    |          |                     |         |          |');
			gotoxy(3,baris + j);writeln(j);
			gotoxy(8,baris + j);writeln(mk[j].kode);
			gotoxy(19,baris + j);writeln(mk[j].nama);
			gotoxy(41,baris + j);writeln(mk[j].sks);
			gotoxy(52,baris + j);writeln(indeks[i,j]);
		end;
		gotoxy(1,baris + j + 1);
		writeln('------------------------------------------------------------');
		gotoxy(1,baris + j + 2);writeln;
		gotoxy(1,baris + j + 3);writeln('IPK :',mhs[i].ipk:0:1);
    baris := baris + j;
	end;
	
end;


begin
  IsiData(m,n,mhs,mk);
  IsiNilai(m,n,mhs,mk,nilai,indeks);
  TampilIndeks(m,n,mhs,mk,nilai,indeks);
  TampilHasil(m,n,mhs,mk,nilai,indeks);
end.
