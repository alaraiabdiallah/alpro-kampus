program Mengelola_nilai_mhs;
uses
    crt;
const
     MaksBaris = 50;
     MaksKolom = 5;
type
    larik1    = array[1..MaksBaris] of string;
    larik2    = array[1..MaksKolom] of string;
    matriks1  = array[1..MaksBaris,1..MaksKolom] of integer;
    matriks2  = array[1..MaksBaris,1..MaksKolom] of char;
    
    MhsRecord = record
       nim,nama : string;
    end;
    Matkul = record
       kode, nama : string;
       sks : integer;
    end;
var
   nim   : larik1;
   mk    : larik2;
   nilai : matriks1;
   idx   : matriks2;
   m,n   : integer;

procedure Validasi(var m,n:integer);
begin
     write('Masukkan Jumlah Mahasiswa : ');readln(m);
     while (m <= 0) or (m > MaksBaris) do
     begin
        writeln('Jumlah Mahasiswa mulai dari 1 - ',MaksBaris);
        readln;
        gotoxy(1,2);clreol;gotoxy(1,1);clreol;
        write('Masukkan Jumlah Mahasiswa : ');readln(m);
     end;
     write('Masukkan Jumlah Mata Kuliah : ');readln(n);
     while (n <= 0) or (n > MaksKolom) do
     begin
        writeln('Jumlah Mata Kuliah mulai dari 1 - ',MaksKolom);
        readln;
        gotoxy(1,3);clreol;gotoxy(1,2);clreol;
        write('Masukkan Jumlah Mata Kuliah : ');readln(n);
     end;
end;

{procedure IsiData(m,n:integer;nim:larik1;mk:larik2;nilai:matriks1;idx:matriks2);
var
   i,j:integer;
begin
   clrscr;
   gotoxy(13,2);write('Kode Mata Kuliah');
   gotoxy(1,3);write('NIM');
   {for i := 1 to 5 do
   begin
       for j := 1 to 3 do
       begin
           gotoxy(j*9-8,i+2);readln(a[i,j]);
       end;
   end;}
   for i := 1 to n do
   begin
       gotoxy(i*15-2,3);readln(mk[n]);
   end;

   for i := 1 to m do
   begin
       gotoxy(1,i*10-4);readln(nim[m]);
   end;
end;}

begin
   Validasi(m,n);
   //IsiData(m,n,nim,mk,nilai,idx);
   //TampilMatriks(a);
   readln;
end.

